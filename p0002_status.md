# git status: Conferindo o estado atual do Git

Para conferir em que situação se encontra o seu repositório, usamos o comando:

```
$ git status
```

- **$** indica que você deve usar o **usuário comum** para fazer essa operação.

Vários retornos são possíveis com esse comando. A depender da situação.

Exemplos:

- Quando não há nenhum arquivo ou modificação nova no diretório/pasta.

![nenhum commit](img/p0002-0.png)

- Quando um arquivo já rastreado foi renomeado.

![arquivo renomeado](img/p0002-1.png)

- Quando um arquivo foi modificado.

![arquivo modificado](img/p0002-2.png)

- Quando criado um arquivo novo no diretório/pasta.

![novo arquivo](img/p0002-3.png)

- Quando foi feito o commit localmente, mas ainda não foi enviado ao repositório remoto.

![submissões dos commit ao repositório remoto](img/p0002-4.png)

Entre outros.

tags: git, status, tutorial, linux,
