# Tutorial Git


0. [Instalando, configurando e inicializando o Git no Linux](p0000_instalacao_linux.md)

1. [git add: Adicionando arquivos no Git](p0001_add.md)

2. [git status: Conferindo o estado atual do Git](p0002_status.md)

3. [git rm/reset: Removendo arquivos do index](p0003_remove_add.md)

4. [git commit: Enviando arquivos para o repositório Git](p0004_commit.md)

5. [git log/reflog: Verificando o histórico do git](p0005_log_reflog.md)

6. [git commit --amend: Corrigindo o último commit](p0006_amend.md)

7. [git mv: Renomeando ou movendo arquivos no Git](p0007_renomear_mover.md)

8. [git diff: Verificando modificações no repositório](p0008_diff.md)

9. [git commit -am: Atualizando arquivo modificado no Git](p0009_am.md)

10. [gitignore: Ignorando arquivos no Git](p0010_ignore.md)

11. [Visualizando o commit no modo gráfico](p0011_grafico_terminal.md)

12. [git rm: Deletando arquivo do repositório](p0012_delete_arquivo.md)

13. [git tag: Etiquetas (tags) no Git](p0013_tag.md)

14. [git branch: O que são branches (ramos) no Git?](p0014_branch.md)

15. [git checkout: Navegando entre ramos e snapshots, e descartando modificações](p0015_checkout.md)

16. [git reset: Retirando arquivos da "sala de espera" e desfazendo commits](p0016_reset.md)

17. [git revert: Desfazendo commits](p0017_revert_commit.md)

18. [git push: Usando o Github e enviando um projeto local para o repositório remoto](p0018_push.md)

19. [git fetch: Verificando commits remotos](p0019_fetch.md)