# git mv: Renomeando ou movendo arquivos no Git


Para renomear um arquivo do repositório, basta digitar o comando:

```
$ git mv <nome_antigo> <nome_novo>
```

- o símbolo **$** indica que você deve usar o **usuário comum** para fazer essa operação.
- substitua o \<nome_antigo> e \<nome_novo> pelos nomes do arquivo e o novo nome que deseja dar a ele, sem os sinais **<>**

Esse comando também é usado para mover arquivos do repositório. Exemplo:

```
$ git mv <arquivo> <diretorio/>
```

Exemplo:

Mudando o nome do arquivo **nintendo64.html** para **n64.html**.

![mudando o nome do arquivo nintendo64.html](img/p0007-0.png)

Movendo o arquivo **estilo.css** para o **diretório css**.

![movendo o arquivo estilo.css](img/p0007-1.png)

tags: git, renomear, move, mv, linux, tutorial
